## Checklist for starting the course

* Create an account on https://gitlab.kwant-project.org<br>
  **IMPORTANT: Please use your tudelft email address!**
* [Request membership](howtos/request_access.md) in the [course team](https://gitlab.kwant-project.org/computational_physics_19)
* [Try accessing your account](howtos/first_time_jupyter.md) on https://compphys.quantumtinkerer.tudelft.nl
* Join the course chat channel: https://chat.quantumtinkerer.tudelft.nl/compphys
* Create a [git project](howtos/HOWTO_git.md) for the first simulation

## Important dates

[Schedule of the course and deadlines](schedule.md) 

## Evaluation criteria

[Criteria and grading scheme](grading_scheme.md) for projects 1 and 2.

## Detailed descriptions

#### Useful technical background material

[Resources for learning python and git](howtos/casimir_prog_course.md)

[Some basic steps for using repositories in gitlab](howtos/HOWTO_git.md)

#### Project 1

[Project 1 - description](project 1/description.md)

[Project 1 - lecture notes](project 1/project.md)

#### Project 2

[Introduction to Monte Carlo methods](project 2/montecarlo_intro.pdf)

[Project 2 - descriptions](project 2/projects.md)

[Notebook explaining how you can use cython to speed up your code](https://gitlab.kwant-project.org/computational_physics_18/course_notes/raw/master/python_examples/Speeding%20up%20your%20python%20calculations.ipynb)
