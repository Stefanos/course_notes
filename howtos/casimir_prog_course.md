# Resources for learning python and git

### General remarks

There is a huge amount of (very good) information on python basics and advanced python use, as well as git on the internet. If in doubt, first try to google it!

### Programming crash course

We have prepared a two-day scientific computing crash course which is available online
at https://gitlab.kwant-project.org/computational_physics_19/computing_crash_course.

Since the material is jupyter notebooks, they are most useful if opened in a jupyterhub server. You can of course do this locally on your computer. Here I describe how to do it on the [jupyter server of the course](https://compphys.quantumtinkerer.tudelft.nl/).

1. Go to https://compphys.quantumtinkerer.tudelft.nl/

2. Log in and start your server, if you are prompted to do so.

3. Click on "New -> Terminal"
![](new_terminal.png)

4. A terminal will now open in a new browser tab. Type in `git clone https://gitlab.kwant-project.org/computational_physics_19/computing_crash_course` and hit ENTER

5. Switch back to the browser tab with the home folder of your jupyter server. You will now see the programming course:
![](clone_crash_course.png)

6. Click your way through the folders to find all the notebooks:
   - general python programming is covered in `day1_morning`, `day1_afternoon`, and `structuring_python_code`
   - git is covered in `day2_afternoon`. Note that there Github is used, but you should find it easy to translate everything to Gitlab (also have a look at [our gitlab HOWTO](HOWTO_git.md))

**IMPORTANT** We advise to use this programming course mainly as a reference. There are also a lot of exercises. There is no need to do all of these, practice your programming skills directly on the simulation project!

